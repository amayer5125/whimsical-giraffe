# WhimsicalGiraffe

A php package to generate random adjective-noun pairs useful for naming servers and other things.

Generated strings are in the form "adjective-noun".

## Installation

```sh
composer require amayer5125/whimsical-giraffe
```

## Usage

```php
use amayer5125\WhimsicalGiraffe\WhimsicalGiraffe;

echo WhimsicalGiraffe::generate();
```
