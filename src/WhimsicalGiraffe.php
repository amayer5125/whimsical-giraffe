<?php

/**
 * This file is part of the WhimsicalGiraffe package.
 *
 * (c) Alex Mayer <amayer5125@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace amayer5125\WhimsicalGiraffe;

class WhimsicalGiraffe
{
    /**
     * A list of adjectives.
     *
     * @var array
     */
    private static $adjectives = [];

    /**
     * A list of nouns.
     *
     * @var array
     */
    private static $nouns = [];

    /**
     * Generate a whimsical adjective noun pair.
     *
     * @return string
     */
    public static function generate()
    {
        // populate adjectives if they haven't been already
        if (empty(self::$adjectives)) {
            self::$adjectives = require_once 'Adjectives.php';
        }

        // populate nouns if they haven't been already
        if (empty(self::$nouns)) {
            self::$nouns = require_once 'Nouns.php';
        }

        // get random adjective
        $adjective = self::$adjectives[array_rand(self::$adjectives)];

        // get a random noun
        $noun = self::$nouns[array_rand(self::$nouns)];

        // return adjective-noun pair
        return "$adjective-$noun";
    }
}
